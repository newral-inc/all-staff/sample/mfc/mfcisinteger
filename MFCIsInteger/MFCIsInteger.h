﻿
// MFCIsInteger.h : PROJECT_NAME アプリケーションのメイン ヘッダー ファイルです
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CMFCIsIntegerApp:
// このクラスの実装については、MFCIsInteger.cpp を参照してください
//

class CMFCIsIntegerApp : public CWinApp
{
public:
	CMFCIsIntegerApp();

// オーバーライド
public:
	virtual BOOL InitInstance();

// 実装

	DECLARE_MESSAGE_MAP()
};

extern CMFCIsIntegerApp theApp;
