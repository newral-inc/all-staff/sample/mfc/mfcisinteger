
#include "stdafx.h"
#include <afxwin.h>
#include <Windows.h>
#include <regex>

BOOL IsInteger(CString value)
{
	std::wregex pattern(_T("^(-)?([1-9]?[0-9]*|0)$"));
	std::wsmatch sm;

	std::wstring src(value.Trim());

	if (std::regex_match(src, sm, pattern))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
